<?php
// Example of connection settings.
$connections = [
  'db_host' => 'localhost',
  'db_name' => '{database_name}',
  'db_user' => '{user_name}',
  'db_password' => '{password}',
  'db_port' => '{port}',
];

// Load local configuration, if available.
if (file_exists(__DIR__ . '/connections.local.php')) {
  include __DIR__ . '/connections.local.php';
}