# QuantoxSchoolBoard

App for getting student test results.

Entry point for this app is in `public/index.php`.

# Requirements

This app needs at least php 7 to function.
This app need a MySQL database to function.
This app needs rewrite rules allowed if using NGINX. For apache there is already a .htaccess file but the module mod_rewrite needs to be active.
Composer install is required before this app can function.
 
# Settings

There are default/placeholders for database connection in the file `config/connections.php`.
However if there is a file `connections.local.php` in the folder then the values from that file will be used.
All files in the `config` directory that have the extension `.local.php` are excluded from the git.