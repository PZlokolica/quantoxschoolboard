<?php
require_once __DIR__ . '/../config/connections.php';
require __DIR__ . '/../vendor/autoload.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SchoolBoard\Services\EntityLoader;

$request = Request::createFromGlobals();

// Get th path info
$path_info = $request->getPathInfo();
//Remove the trailing slash.
$path_info = ltrim($path_info, '/');

// Get the response.
if ($path_info) {
  if (strpos($path_info, 'users') !== 0) {
    $response = new Response('Page not found!');
    $response->setStatusCode(404);
  }
  else {
    $id = str_replace('users/', '', $path_info);
    $id = intval($id);

    // Get the entity loader and get to which school board it belongs to.
    $user_school_board = new EntityLoader();

    $user_school_board = $user_school_board->getEntityProperty('student', $id, 'board');

    if ($user_school_board === 'CSM') {
      $response = new \SchoolBoard\Controller\CSMController($id);
      $response = $response->returnResponse();
    }
    if ($user_school_board === 'CSMB') {
      $response = new \SchoolBoard\Controller\CSMBController($id);
      $response = $response->returnResponse();
    }
  }
}
else {
  $response = new \SchoolBoard\Controller\FrontController();
  $response = $response->returnResponse();
}

$response->send();