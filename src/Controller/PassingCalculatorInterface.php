<?php
namespace SchoolBoard\Controller;

interface PassingCalculatorInterface {
  public function calculatePassing(array $grades);
}
