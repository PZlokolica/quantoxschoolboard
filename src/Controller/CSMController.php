<?php
namespace SchoolBoard\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class CSMController extends BaseController implements PassingCalculatorInterface {

  public $student_id;

  public function __construct($student_id) {
    $this->student_id = $student_id;
    parent::__construct();
  }

  public function returnResponse() {
    // Load the student.
    $student = $this->loader->loadEntity('student', $this->student_id);

    $grades = [
      $student->grade1,
      $student->grade2,
      $student->grade3,
      $student->grade4
    ];

    // If the student exists return a response else throw an exception.
    if ($student) {
      // Get the final result and cast it to string.
      $final_result = $this->calculatePassing($grades);
      // Set the result and the average.
      if ($final_result) {
        $student->final_result = 'Pass';
      }
      else {
        $student->final_result = 'Fail';
      }
      $student->average = $this->calculateAverage($grades);

      return new JsonResponse($student);
    }
    else {
      throw new \Exception ('The specified student does not exist.');
    }

  }


  public function calculatePassing(array $grades) {
    $average = $this->calculateAverage($grades);
    $average >= 7 ? TRUE : FALSE;
  }
}