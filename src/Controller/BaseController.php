<?php
namespace SchoolBoard\Controller;

use SchoolBoard\Services\EntityLoader;

abstract class BaseController implements ControllerInterface {

  public $loader;

  public function __construct() {
    $this->loader = new EntityLoader();
  }

  /**
   * Get the average of the supplied numbers.
   *
   * @param array $numbers
   *
   * @return float
   */
  public static function calculateAverage(array $numbers) {
    // Remove null and empty string from the array.
    $clean_values = self::clearArrayOfNull($numbers);

    return array_sum($clean_values) / count($clean_values);
  }

  public static function clearArrayOfNull(array $array) {
    // Remove null and empty string from the array.
    $clean_values = [];
    foreach ($array as $value) {
      if ($value !== '' && $value !== NULL) {
        $clean_values[] = $value;
      }
    }
    return $clean_values;
  }
}