<?php
namespace SchoolBoard\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class CSMBController extends BaseController implements PassingCalculatorInterface{


  public $student_id;


  public function __construct($student_id) {
    $this->student_id = $student_id;
    parent::__construct();
  }

  public function returnResponse() {
    $student = $this->loader->loadEntity('student', $this->student_id);

    $grades = [
      $student->grade1,
      $student->grade2,
      $student->grade3,
      $student->grade4
    ];

    if ($student) {
      $final_result = $this->calculatePassing($grades);
      if ($final_result) {
        $student->final_result = 'Pass';
      }
      else {
        $student->final_result = 'Fail';
      }

      $grades = $this->clearArrayOfNull($grades);
      if (count($grades) > 2) {
        // Remove the smallest grade if there are more then two grades
        unset($grades[array_search(min($grades), $grades)]);
      }

      // Get the average.
      $student->average = $this->calculateAverage($grades);

      $encoder = new XmlEncoder();

      $data = $encoder->encode($student, XmlEncoder::FORMAT);

      $response = new Response($data);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    else {
      throw new \Exception ('The specified student does not exist.');
    }
  }

  public function calculatePassing(array $grades) {
    if (max($grades) > 8) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}