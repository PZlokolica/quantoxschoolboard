<?php
namespace SchoolBoard\Controller;

use Symfony\Component\HttpFoundation\Response;

class FrontController extends BaseController {

  public function returnResponse() {
    $response = new Response('<h1>Welcome to the app!</h1>
      <p> Please navigate to the user to see the data. There are user with ids from 1 to 5.</p>
      <p>Example users/1</p>');
    return $response;
  }

}