<?php
namespace SchoolBoard\Controller;

use Symfony\Component\HttpFoundation\Response;

interface ControllerInterface {
  public function returnResponse();
}