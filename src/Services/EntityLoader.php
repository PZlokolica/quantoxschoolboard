<?php

namespace SchoolBoard\Services;

use SchoolBoard\Database\Database;

class EntityLoader {

  private $connection;

  public function __construct() {
    // Get the connection in order to get the entities.
    $this->connection = Database::getConnection();
  }

  /**
   * Function that loads the entities by their ID.
   *
   * @param $entity_database_table_name
   * @param $id
   *
   * @return null|object|\stdClass
   */
  public function loadEntity($entity_database_table_name, $id) {
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);
    $entity = $this->connection->query("SELECT * FROM $entity_database_table_name WHERE id = $id LIMIT 1");
    if ($entity) {
      return $entity->fetch_object();
    }
    else {
      return NULL;
    }
  }

  public function getEntityProperty($entity_database_table_name, $id, $property) {
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);
    $property = $this->connection->escape_string($property);
    $entity = $this->connection->query("SELECT $property FROM $entity_database_table_name WHERE id = $id LIMIT 1");
    if ($entity) {
      $entity = $entity->fetch_assoc();
      return $entity[$property];
    }
    else {
      return NULL;
    }
  }

}