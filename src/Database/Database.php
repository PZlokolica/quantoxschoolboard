<?php
namespace SchoolBoard\Database;

/**
 * Class that is used to connect to the database.
 */
class Database {

  /**
   * Function for getting the specified mysql connection.
   *
   * @param $connection_name  String  Specify which database to connect to. If it is not supplied the default one will be selected.
   * @return \mysqli
   * @throws \Exception
   */
  static public function getConnection($connection_name = NULL) {
    // Get the connections data
    global $connections;
    // Set the parameters.
    $mysql_server = $connection_name ? $connections[$connection_name]['db_host'] : $connections['db_host'];
    $mysql_user = $connection_name ? $connections[$connection_name]['db_user'] : $connections['db_user'];
    $mysql_pass = $connection_name ? $connections[$connection_name]['db_password'] : $connections['db_password'];
    $mysql_database = $connection_name ? $connections[$connection_name]['db_name'] : $connections['db_name'];
    // Try connecting to the database.
    try {
      $conn = new \mysqli($mysql_server, $mysql_user, $mysql_pass, $mysql_database);
      $conn->set_charset('utf8');
      return $conn;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }
}